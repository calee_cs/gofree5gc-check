package main

import (
	"gofree5gc/lib/nas"
	"gofree5gc/lib/nas/nasMessage"
	"testing"
)

func TestPrint(t *testing.T) {
	print("hello")
	m := nas.NewMessage()
	m.GsmMessage = nas.NewGsmMessage()
	m.GsmMessage.PDUSessionEstablishmentRequest = nasMessage.NewPDUSessionEstablishmentRequest(1)
	print("print main\n")
	println(m.GsmMessage.PDUSessionEstablishmentRequest)
}
