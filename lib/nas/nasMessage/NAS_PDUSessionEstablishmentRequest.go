package nasMessage

import (
        "bytes"
        "encoding/binary"
        "fmt"
        "gofree5gc/lib/nas/nasType"
)
type PDUSessionEstablishmentRequest struct {
    nasType.ExtendedProtocolDiscriminator
    nasType.PDUSessionID
    nasType.PTI
    nasType.PDUSESSIONESTABLISHMENTREQUESTMessageIdentity
    nasType.IntegrityProtectionMaximumDataRate
    *nasType.PDUSessionType
    *nasType.SSCMode
    *nasType.Capability5GSM
    *nasType.MaximumNumberOfSupportedPacketFilters
    *nasType.AlwaysonPDUSessionRequested
    *nasType.SMPDUDNRequestContainer
    *nasType.ExtendedProtocolConfigurationOptions
}

func NewPDUSessionEstablishmentRequest(iei uint8) (pDUSessionEstablishmentRequest *PDUSessionEstablishmentRequest) {
    pDUSessionEstablishmentRequest = &PDUSessionEstablishmentRequest{}
    return pDUSessionEstablishmentRequest
}

const (
    PDUSessionEstablishmentRequestPDUSessionTypeType uint8 = 0x90
    PDUSessionEstablishmentRequestSSCModeType uint8 = 0xA0
    PDUSessionEstablishmentRequestCapability5GSMType uint8 = 0x28
    PDUSessionEstablishmentRequestMaximumNumberOfSupportedPacketFiltersType uint8 = 0x55
    PDUSessionEstablishmentRequestAlwaysonPDUSessionRequestedType uint8 = 0xB0
    PDUSessionEstablishmentRequestSMPDUDNRequestContainerType uint8 = 0x39
    PDUSessionEstablishmentRequestExtendedProtocolConfigurationOptionsType uint8 = 0x7B
)

func (a *PDUSessionEstablishmentRequest) EncodePDUSessionEstablishmentRequest(buffer *bytes.Buffer) {
    binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Write(buffer, binary.BigEndian, &a.PDUSessionID)
    binary.Write(buffer, binary.BigEndian, &a.PTI)
    binary.Write(buffer, binary.BigEndian, &a.PDUSESSIONESTABLISHMENTREQUESTMessageIdentity)
    binary.Write(buffer, binary.BigEndian, &a.IntegrityProtectionMaximumDataRate)
    if a.PDUSessionType != nil {
        binary.Write(buffer, binary.BigEndian, a.PDUSessionType.GetIei())
        binary.Write(buffer, binary.BigEndian, &a.PDUSessionType.Octet)
    }
    if a.SSCMode != nil {
        binary.Write(buffer, binary.BigEndian, a.SSCMode.GetIei())
        binary.Write(buffer, binary.BigEndian, &a.SSCMode.Octet)
    }
    if a.Capability5GSM != nil {
        binary.Write(buffer, binary.BigEndian, a.Capability5GSM.GetIei())
        binary.Write(buffer, binary.BigEndian, a.Capability5GSM.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.Capability5GSM.Octet)
    }
    if a.MaximumNumberOfSupportedPacketFilters != nil {
        binary.Write(buffer, binary.BigEndian, a.MaximumNumberOfSupportedPacketFilters.GetIei())
        binary.Write(buffer, binary.BigEndian, &a.MaximumNumberOfSupportedPacketFilters.Octet)
    }
    if a.AlwaysonPDUSessionRequested != nil {
        binary.Write(buffer, binary.BigEndian, a.AlwaysonPDUSessionRequested.GetIei())
        binary.Write(buffer, binary.BigEndian, &a.AlwaysonPDUSessionRequested.Octet)
    }
    if a.SMPDUDNRequestContainer != nil {
        binary.Write(buffer, binary.BigEndian, a.SMPDUDNRequestContainer.GetIei())
        binary.Write(buffer, binary.BigEndian, a.SMPDUDNRequestContainer.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.SMPDUDNRequestContainer.Buffer)
    }
    if a.ExtendedProtocolConfigurationOptions != nil {
        binary.Write(buffer, binary.BigEndian, a.ExtendedProtocolConfigurationOptions.GetIei())
        binary.Write(buffer, binary.BigEndian, a.ExtendedProtocolConfigurationOptions.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Buffer)
    }
}

func (a *PDUSessionEstablishmentRequest) DecodePDUSessionEstablishmentRequest(byteArray *[]byte) {
    buffer := bytes.NewBuffer(*byteArray)
    binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Read(buffer, binary.BigEndian, &a.PDUSessionID)
    binary.Read(buffer, binary.BigEndian, &a.PTI)
    binary.Read(buffer, binary.BigEndian, &a.PDUSESSIONESTABLISHMENTREQUESTMessageIdentity)
    binary.Read(buffer, binary.BigEndian, &a.IntegrityProtectionMaximumDataRate)
    for buffer.Len() > 0 {
        var ieiN uint8
        var tmpIeiN uint8
        binary.Read(buffer, binary.BigEndian, &ieiN)
        fmt.Println(ieiN)
        if ieiN >= 0x80 {
            tmpIeiN = ieiN & 0xf0
        } else {
            tmpIeiN = ieiN
        }
        fmt.Println("type", tmpIeiN)
        switch tmpIeiN  {
        case PDUSessionEstablishmentRequestPDUSessionTypeType:
            a.PDUSessionType = nasType.NewPDUSessionType(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.PDUSessionType.Octet)
        case PDUSessionEstablishmentRequestSSCModeType:
            a.SSCMode = nasType.NewSSCMode(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.SSCMode.Octet)
        case PDUSessionEstablishmentRequestCapability5GSMType:
            a.Capability5GSM = nasType.NewCapability5GSM(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.Capability5GSM.Len)
            a.Capability5GSM.SetLen(a.Capability5GSM.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.Capability5GSM.Octet)
        case PDUSessionEstablishmentRequestMaximumNumberOfSupportedPacketFiltersType:
            a.MaximumNumberOfSupportedPacketFilters = nasType.NewMaximumNumberOfSupportedPacketFilters(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.MaximumNumberOfSupportedPacketFilters.Octet)
        case PDUSessionEstablishmentRequestAlwaysonPDUSessionRequestedType:
            a.AlwaysonPDUSessionRequested = nasType.NewAlwaysonPDUSessionRequested(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.AlwaysonPDUSessionRequested.Octet)
        case PDUSessionEstablishmentRequestSMPDUDNRequestContainerType:
            a.SMPDUDNRequestContainer = nasType.NewSMPDUDNRequestContainer(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.SMPDUDNRequestContainer.Len)
            a.SMPDUDNRequestContainer.SetLen(a.SMPDUDNRequestContainer.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.SMPDUDNRequestContainer.Buffer)
        case PDUSessionEstablishmentRequestExtendedProtocolConfigurationOptionsType:
            a.ExtendedProtocolConfigurationOptions = nasType.NewExtendedProtocolConfigurationOptions(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Len)
            a.ExtendedProtocolConfigurationOptions.SetLen(a.ExtendedProtocolConfigurationOptions.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Buffer)
        default:
        }
    }
}

