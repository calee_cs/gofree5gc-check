package nasMessage

import (
        "bytes"
        "encoding/binary"
        "fmt"
        "gofree5gc/lib/nas/nasType"
)
type PDUSessionModificationReject struct {
    nasType.ExtendedProtocolDiscriminator
    nasType.PDUSessionID
    nasType.PTI
    nasType.PDUSESSIONMODIFICATIONREJECTMessageIdentity
    nasType.Cause5GSM
    *nasType.BackoffTimerValue
    *nasType.ExtendedProtocolConfigurationOptions
}

func NewPDUSessionModificationReject(iei uint8) (pDUSessionModificationReject *PDUSessionModificationReject) {
    pDUSessionModificationReject = &PDUSessionModificationReject{}
    return pDUSessionModificationReject
}

const (
    PDUSessionModificationRejectBackoffTimerValueType uint8 = 0x37
    PDUSessionModificationRejectExtendedProtocolConfigurationOptionsType uint8 = 0x7B
)

func (a *PDUSessionModificationReject) EncodePDUSessionModificationReject(buffer *bytes.Buffer) {
    binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Write(buffer, binary.BigEndian, &a.PDUSessionID)
    binary.Write(buffer, binary.BigEndian, &a.PTI)
    binary.Write(buffer, binary.BigEndian, &a.PDUSESSIONMODIFICATIONREJECTMessageIdentity)
    binary.Write(buffer, binary.BigEndian, &a.Cause5GSM)
    if a.BackoffTimerValue != nil {
        binary.Write(buffer, binary.BigEndian, a.BackoffTimerValue.GetIei())
        binary.Write(buffer, binary.BigEndian, a.BackoffTimerValue.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.BackoffTimerValue.Octet)
    }
    if a.ExtendedProtocolConfigurationOptions != nil {
        binary.Write(buffer, binary.BigEndian, a.ExtendedProtocolConfigurationOptions.GetIei())
        binary.Write(buffer, binary.BigEndian, a.ExtendedProtocolConfigurationOptions.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Buffer)
    }
}

func (a *PDUSessionModificationReject) DecodePDUSessionModificationReject(byteArray *[]byte) {
    buffer := bytes.NewBuffer(*byteArray)
    binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Read(buffer, binary.BigEndian, &a.PDUSessionID)
    binary.Read(buffer, binary.BigEndian, &a.PTI)
    binary.Read(buffer, binary.BigEndian, &a.PDUSESSIONMODIFICATIONREJECTMessageIdentity)
    binary.Read(buffer, binary.BigEndian, &a.Cause5GSM)
    for buffer.Len() > 0 {
        var ieiN uint8
        var tmpIeiN uint8
        binary.Read(buffer, binary.BigEndian, &ieiN)
        fmt.Println(ieiN)
        if ieiN >= 0x80 {
            tmpIeiN = ieiN & 0xf0
        } else {
            tmpIeiN = ieiN
        }
        fmt.Println("type", tmpIeiN)
        switch tmpIeiN  {
        case PDUSessionModificationRejectBackoffTimerValueType:
            a.BackoffTimerValue = nasType.NewBackoffTimerValue(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.BackoffTimerValue.Len)
            a.BackoffTimerValue.SetLen(a.BackoffTimerValue.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.BackoffTimerValue.Octet)
        case PDUSessionModificationRejectExtendedProtocolConfigurationOptionsType:
            a.ExtendedProtocolConfigurationOptions = nasType.NewExtendedProtocolConfigurationOptions(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Len)
            a.ExtendedProtocolConfigurationOptions.SetLen(a.ExtendedProtocolConfigurationOptions.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Buffer)
        default:
        }
    }
}

