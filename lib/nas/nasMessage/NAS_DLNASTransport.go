package nasMessage

import (
        "bytes"
        "encoding/binary"
        "fmt"
        "gofree5gc/lib/nas/nasType"
)
type DLNASTransport struct {
    nasType.ExtendedProtocolDiscriminator
    nasType.SecurityHeaderTypeAndSpareHalfOctet
    nasType.DLNASTRANSPORTMessageIdentity
    nasType.PayloadContainerTypeAndSpareHalfOctet
    nasType.PayloadContainer
    *nasType.PDUSessionID
    *nasType.AdditionalInformation
    *nasType.Cause5GMM
    *nasType.BackoffTimerValue
}

func NewDLNASTransport(iei uint8) (dLNASTransport *DLNASTransport) {
    dLNASTransport = &DLNASTransport{}
    return dLNASTransport
}

const (
    DLNASTransportPDUSessionIDType uint8 = 0x12
    DLNASTransportAdditionalInformationType uint8 = 0x24
    DLNASTransportCause5GMMType uint8 = 0x58
    DLNASTransportBackoffTimerValueType uint8 = 0x37
)

func (a *DLNASTransport) EncodeDLNASTransport(buffer *bytes.Buffer) {
    binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Write(buffer, binary.BigEndian, &a.SecurityHeaderTypeAndSpareHalfOctet)
    binary.Write(buffer, binary.BigEndian, &a.DLNASTRANSPORTMessageIdentity)
    binary.Write(buffer, binary.BigEndian, &a.PayloadContainerTypeAndSpareHalfOctet)
    binary.Write(buffer, binary.BigEndian, a.PayloadContainer.GetLen())
    binary.Write(buffer, binary.BigEndian, &a.PayloadContainer.Buffer)
    if a.PDUSessionID != nil {
        binary.Write(buffer, binary.BigEndian, a.PDUSessionID.GetIei())
        binary.Write(buffer, binary.BigEndian, &a.PDUSessionID.Octet)
    }
    if a.AdditionalInformation != nil {
        binary.Write(buffer, binary.BigEndian, a.AdditionalInformation.GetIei())
        binary.Write(buffer, binary.BigEndian, a.AdditionalInformation.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.AdditionalInformation.Buffer)
    }
    if a.Cause5GMM != nil {
        binary.Write(buffer, binary.BigEndian, a.Cause5GMM.GetIei())
        binary.Write(buffer, binary.BigEndian, &a.Cause5GMM.Octet)
    }
    if a.BackoffTimerValue != nil {
        binary.Write(buffer, binary.BigEndian, a.BackoffTimerValue.GetIei())
        binary.Write(buffer, binary.BigEndian, a.BackoffTimerValue.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.BackoffTimerValue.Octet)
    }
}

func (a *DLNASTransport) DecodeDLNASTransport(byteArray *[]byte) {
    buffer := bytes.NewBuffer(*byteArray)
    binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Read(buffer, binary.BigEndian, &a.SecurityHeaderTypeAndSpareHalfOctet)
    binary.Read(buffer, binary.BigEndian, &a.DLNASTRANSPORTMessageIdentity)
    binary.Read(buffer, binary.BigEndian, &a.PayloadContainerTypeAndSpareHalfOctet)
    binary.Read(buffer, binary.BigEndian, &a.PayloadContainer.Len)
    a.PayloadContainer.SetLen(a.PayloadContainer.GetLen())
    binary.Write(buffer, binary.BigEndian, &a.PayloadContainer.Buffer)
    for buffer.Len() > 0 {
        var ieiN uint8
        var tmpIeiN uint8
        binary.Read(buffer, binary.BigEndian, &ieiN)
        fmt.Println(ieiN)
        if ieiN >= 0x80 {
            tmpIeiN = ieiN & 0xf0
        } else {
            tmpIeiN = ieiN
        }
        fmt.Println("type", tmpIeiN)
        switch tmpIeiN  {
        case DLNASTransportAdditionalInformationType:
            a.AdditionalInformation = nasType.NewAdditionalInformation(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.AdditionalInformation.Len)
            a.AdditionalInformation.SetLen(a.AdditionalInformation.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.AdditionalInformation.Buffer)
        case DLNASTransportCause5GMMType:
            a.Cause5GMM = nasType.NewCause5GMM(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.Cause5GMM.Octet)
        case DLNASTransportBackoffTimerValueType:
            a.BackoffTimerValue = nasType.NewBackoffTimerValue(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.BackoffTimerValue.Len)
            a.BackoffTimerValue.SetLen(a.BackoffTimerValue.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.BackoffTimerValue.Octet)
        default:
        }
    }
}

