package nasMessage

import (
        "bytes"
        "encoding/binary"
        "fmt"
        "gofree5gc/lib/nas/nasType"
)
type PDUSessionAuthenticationResult struct {
    nasType.ExtendedProtocolDiscriminator
    nasType.PDUSessionID
    nasType.PTI
    nasType.PDUSESSIONAUTHENTICATIONRESULTMessageIdentity
    *nasType.EAPMessage
    *nasType.ExtendedProtocolConfigurationOptions
}

func NewPDUSessionAuthenticationResult(iei uint8) (pDUSessionAuthenticationResult *PDUSessionAuthenticationResult) {
    pDUSessionAuthenticationResult = &PDUSessionAuthenticationResult{}
    return pDUSessionAuthenticationResult
}

const (
    PDUSessionAuthenticationResultEAPMessageType uint8 = 0x78
    PDUSessionAuthenticationResultExtendedProtocolConfigurationOptionsType uint8 = 0x7B
)

func (a *PDUSessionAuthenticationResult) EncodePDUSessionAuthenticationResult(buffer *bytes.Buffer) {
    binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Write(buffer, binary.BigEndian, &a.PDUSessionID)
    binary.Write(buffer, binary.BigEndian, &a.PTI)
    binary.Write(buffer, binary.BigEndian, &a.PDUSESSIONAUTHENTICATIONRESULTMessageIdentity)
    if a.EAPMessage != nil {
        binary.Write(buffer, binary.BigEndian, a.EAPMessage.GetIei())
        binary.Write(buffer, binary.BigEndian, a.EAPMessage.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.EAPMessage.Buffer)
    }
    if a.ExtendedProtocolConfigurationOptions != nil {
        binary.Write(buffer, binary.BigEndian, a.ExtendedProtocolConfigurationOptions.GetIei())
        binary.Write(buffer, binary.BigEndian, a.ExtendedProtocolConfigurationOptions.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Buffer)
    }
}

func (a *PDUSessionAuthenticationResult) DecodePDUSessionAuthenticationResult(byteArray *[]byte) {
    buffer := bytes.NewBuffer(*byteArray)
    binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Read(buffer, binary.BigEndian, &a.PDUSessionID)
    binary.Read(buffer, binary.BigEndian, &a.PTI)
    binary.Read(buffer, binary.BigEndian, &a.PDUSESSIONAUTHENTICATIONRESULTMessageIdentity)
    for buffer.Len() > 0 {
        var ieiN uint8
        var tmpIeiN uint8
        binary.Read(buffer, binary.BigEndian, &ieiN)
        fmt.Println(ieiN)
        if ieiN >= 0x80 {
            tmpIeiN = ieiN & 0xf0
        } else {
            tmpIeiN = ieiN
        }
        fmt.Println("type", tmpIeiN)
        switch tmpIeiN  {
        case PDUSessionAuthenticationResultEAPMessageType:
            a.EAPMessage = nasType.NewEAPMessage(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.EAPMessage.Len)
            a.EAPMessage.SetLen(a.EAPMessage.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.EAPMessage.Buffer)
        case PDUSessionAuthenticationResultExtendedProtocolConfigurationOptionsType:
            a.ExtendedProtocolConfigurationOptions = nasType.NewExtendedProtocolConfigurationOptions(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Len)
            a.ExtendedProtocolConfigurationOptions.SetLen(a.ExtendedProtocolConfigurationOptions.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Buffer)
        default:
        }
    }
}

