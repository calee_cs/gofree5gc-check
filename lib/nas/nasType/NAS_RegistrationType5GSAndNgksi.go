package nasType

// RegistrationType5GSAndNgksi 9.11.3.7 9.11.3.32
// FOR  Row, sBit, len = [0, 0], 8 , 1
// RegistrationType5GS Row, sBit, len = [0, 0], 7 , 3
// TSC Row, sBit, len = [0, 0], 4 , 1
// NasKeySetIdentifiler Row, sBit, len = [0, 0], 3 , 3
type RegistrationType5GSAndNgksi struct {
	Octet uint8
}

func NewRegistrationType5GSAndNgksi() (registrationType5GSAndNgksi *RegistrationType5GSAndNgksi) {
    registrationType5GSAndNgksi = &RegistrationType5GSAndNgksi{}
    return registrationType5GSAndNgksi
}

// RegistrationType5GSAndNgksi 9.11.3.7 9.11.3.32
// FOR Row, sBit, len = [0, 0], 8 , 1
func (a *RegistrationType5GSAndNgksi) GetFOR() (fOR uint8) {
    return a.Octet & GetBitMask(8, 7)
}

// RegistrationType5GSAndNgksi 9.11.3.7 9.11.3.32
// FOR Row, sBit, len = [0, 0], 8 , 1
func (a *RegistrationType5GSAndNgksi) SetFOR(fOR uint8) {
    a.Octet = (a.Octet & 127) + ((fOR & 1) << 7)
}

// RegistrationType5GSAndNgksi 9.11.3.7 9.11.3.32
// RegistrationType5GS Row, sBit, len = [0, 0], 7 , 3
func (a *RegistrationType5GSAndNgksi) GetRegistrationType5GS() (registrationType5GS uint8) {
    return a.Octet & GetBitMask(7, 4)
}

// RegistrationType5GSAndNgksi 9.11.3.7 9.11.3.32
// RegistrationType5GS Row, sBit, len = [0, 0], 7 , 3
func (a *RegistrationType5GSAndNgksi) SetRegistrationType5GS(registrationType5GS uint8) {
    a.Octet = (a.Octet & 143) + ((registrationType5GS & 7) << 4)
}

// RegistrationType5GSAndNgksi 9.11.3.7 9.11.3.32
// TSC Row, sBit, len = [0, 0], 4 , 1
func (a *RegistrationType5GSAndNgksi) GetTSC() (tSC uint8) {
    return a.Octet & GetBitMask(4, 3)
}

// RegistrationType5GSAndNgksi 9.11.3.7 9.11.3.32
// TSC Row, sBit, len = [0, 0], 4 , 1
func (a *RegistrationType5GSAndNgksi) SetTSC(tSC uint8) {
    a.Octet = (a.Octet & 247) + ((tSC & 1) << 3)
}

// RegistrationType5GSAndNgksi 9.11.3.7 9.11.3.32
// NasKeySetIdentifiler Row, sBit, len = [0, 0], 3 , 3
func (a *RegistrationType5GSAndNgksi) GetNasKeySetIdentifiler() (nasKeySetIdentifiler uint8) {
    return a.Octet & GetBitMask(3, 0)
}

// RegistrationType5GSAndNgksi 9.11.3.7 9.11.3.32
// NasKeySetIdentifiler Row, sBit, len = [0, 0], 3 , 3
func (a *RegistrationType5GSAndNgksi) SetNasKeySetIdentifiler(nasKeySetIdentifiler uint8) {
    a.Octet = (a.Octet & 248) + (nasKeySetIdentifiler & 7)
}

