package nasType

// AccessTypeAndSpareHalfOctet 9.11.3.11 9.5
// AccessType Row, sBit, len = [0, 0], 7 , 3
type AccessTypeAndSpareHalfOctet struct {
	Octet uint8
}

func NewAccessTypeAndSpareHalfOctet() (accessTypeAndSpareHalfOctet *AccessTypeAndSpareHalfOctet) {
    accessTypeAndSpareHalfOctet = &AccessTypeAndSpareHalfOctet{}
    return accessTypeAndSpareHalfOctet
}

// AccessTypeAndSpareHalfOctet 9.11.3.11 9.5
// AccessType Row, sBit, len = [0, 0], 7 , 3
func (a *AccessTypeAndSpareHalfOctet) GetAccessType() (accessType uint8) {
    return a.Octet & GetBitMask(7, 4)
}

// AccessTypeAndSpareHalfOctet 9.11.3.11 9.5
// AccessType Row, sBit, len = [0, 0], 7 , 3
func (a *AccessTypeAndSpareHalfOctet) SetAccessType(accessType uint8) {
    a.Octet = (a.Octet & 143) + ((accessType & 7) << 4)
}

