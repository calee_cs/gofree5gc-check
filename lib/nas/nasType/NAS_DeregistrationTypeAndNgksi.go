package nasType

// DeregistrationTypeAndNgksi 9.11.3.20 9.11.3.32
// SwitchOff Row, sBit, len = [0, 0], 8 , 1
// ReRegistrationRequired Row, sBit, len = [0, 0], 7 , 1
// AccessType Row, sBit, len = [0, 0], 6 , 2
// TSC Row, sBit, len = [0, 0], 4 , 1
// NasKeySetIdentifiler Row, sBit, len = [0, 0], 3 , 3
type DeregistrationTypeAndNgksi struct {
	Octet uint8
}

func NewDeregistrationTypeAndNgksi() (deregistrationTypeAndNgksi *DeregistrationTypeAndNgksi) {
    deregistrationTypeAndNgksi = &DeregistrationTypeAndNgksi{}
    return deregistrationTypeAndNgksi
}

// DeregistrationTypeAndNgksi 9.11.3.20 9.11.3.32
// SwitchOff Row, sBit, len = [0, 0], 8 , 1
func (a *DeregistrationTypeAndNgksi) GetSwitchOff() (switchOff uint8) {
    return a.Octet & GetBitMask(8, 7)
}

// DeregistrationTypeAndNgksi 9.11.3.20 9.11.3.32
// SwitchOff Row, sBit, len = [0, 0], 8 , 1
func (a *DeregistrationTypeAndNgksi) SetSwitchOff(switchOff uint8) {
    a.Octet = (a.Octet & 127) + ((switchOff & 1) << 7)
}

// DeregistrationTypeAndNgksi 9.11.3.20 9.11.3.32
// ReRegistrationRequired Row, sBit, len = [0, 0], 7 , 1
func (a *DeregistrationTypeAndNgksi) GetReRegistrationRequired() (reRegistrationRequired uint8) {
    return a.Octet & GetBitMask(7, 6)
}

// DeregistrationTypeAndNgksi 9.11.3.20 9.11.3.32
// ReRegistrationRequired Row, sBit, len = [0, 0], 7 , 1
func (a *DeregistrationTypeAndNgksi) SetReRegistrationRequired(reRegistrationRequired uint8) {
    a.Octet = (a.Octet & 191) + ((reRegistrationRequired & 1) << 6)
}

// DeregistrationTypeAndNgksi 9.11.3.20 9.11.3.32
// AccessType Row, sBit, len = [0, 0], 6 , 2
func (a *DeregistrationTypeAndNgksi) GetAccessType() (accessType uint8) {
    return a.Octet & GetBitMask(6, 4)
}

// DeregistrationTypeAndNgksi 9.11.3.20 9.11.3.32
// AccessType Row, sBit, len = [0, 0], 6 , 2
func (a *DeregistrationTypeAndNgksi) SetAccessType(accessType uint8) {
    a.Octet = (a.Octet & 207) + ((accessType & 3) << 4)
}

// DeregistrationTypeAndNgksi 9.11.3.20 9.11.3.32
// TSC Row, sBit, len = [0, 0], 4 , 1
func (a *DeregistrationTypeAndNgksi) GetTSC() (tSC uint8) {
    return a.Octet & GetBitMask(4, 3)
}

// DeregistrationTypeAndNgksi 9.11.3.20 9.11.3.32
// TSC Row, sBit, len = [0, 0], 4 , 1
func (a *DeregistrationTypeAndNgksi) SetTSC(tSC uint8) {
    a.Octet = (a.Octet & 247) + ((tSC & 1) << 3)
}

// DeregistrationTypeAndNgksi 9.11.3.20 9.11.3.32
// NasKeySetIdentifiler Row, sBit, len = [0, 0], 3 , 3
func (a *DeregistrationTypeAndNgksi) GetNasKeySetIdentifiler() (nasKeySetIdentifiler uint8) {
    return a.Octet & GetBitMask(3, 0)
}

// DeregistrationTypeAndNgksi 9.11.3.20 9.11.3.32
// NasKeySetIdentifiler Row, sBit, len = [0, 0], 3 , 3
func (a *DeregistrationTypeAndNgksi) SetNasKeySetIdentifiler(nasKeySetIdentifiler uint8) {
    a.Octet = (a.Octet & 248) + (nasKeySetIdentifiler & 7)
}

