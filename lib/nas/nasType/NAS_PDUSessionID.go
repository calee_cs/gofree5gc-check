package nasType

// PDUSessionID 9.11.3.41
// PduSessionIdentity2Value Row, sBit, len = [0, 0], 8 , 8
type PDUSessionID struct {
	Iei   uint8
	Octet uint8
}

func NewPDUSessionID(iei uint8) (pDUSessionID *PDUSessionID) {
    pDUSessionID = &PDUSessionID{}
    pDUSessionID.SetIei(iei)
    return pDUSessionID
}

// PDUSessionID 9.11.3.41
// Iei Row, sBit, len = [], 8, 8
func (a *PDUSessionID) GetIei() (iei uint8) {
    return a.Iei
}

// PDUSessionID 9.11.3.41
// Iei Row, sBit, len = [], 8, 8
func (a *PDUSessionID) SetIei(iei uint8) {
    a.Iei = iei
}

// PDUSessionID 9.11.3.41
// PduSessionIdentity2Value Row, sBit, len = [0, 0], 8 , 8
func (a *PDUSessionID) GetPduSessionIdentity2Value() (pduSessionIdentity2Value uint8) {
    return a.Octet
}

// PDUSessionID 9.11.3.41
// PduSessionIdentity2Value Row, sBit, len = [0, 0], 8 , 8
func (a *PDUSessionID) SetPduSessionIdentity2Value(pduSessionIdentity2Value uint8) {
    a.Octet = pduSessionIdentity2Value
}

