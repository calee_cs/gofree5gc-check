package nasType

// NgksiAndSpareHalfOctet 9.11.3.32 9.5
// TSC Row, sBit, len = [0, 0], 8 , 7
// NasKeySetIdentifiler Row, sBit, len = [0, 0], 7 , 3
// SpareHalfOctet Row, sBit, len = [0, 0], 4 , 4
type NgksiAndSpareHalfOctet struct {
	Octet uint8
}

func NewNgksiAndSpareHalfOctet() (ngksiAndSpareHalfOctet *NgksiAndSpareHalfOctet) {
    ngksiAndSpareHalfOctet = &NgksiAndSpareHalfOctet{}
    return ngksiAndSpareHalfOctet
}

// NgksiAndSpareHalfOctet 9.11.3.32 9.5
// TSC Row, sBit, len = [0, 0], 8 , 7
func (a *NgksiAndSpareHalfOctet) GetTSC() (tSC uint8) {
    return a.Octet & GetBitMask(8, 1)
}

// NgksiAndSpareHalfOctet 9.11.3.32 9.5
// TSC Row, sBit, len = [0, 0], 8 , 7
func (a *NgksiAndSpareHalfOctet) SetTSC(tSC uint8) {
    a.Octet = (a.Octet & 1) + ((tSC & 127) << 1)
}

// NgksiAndSpareHalfOctet 9.11.3.32 9.5
// NasKeySetIdentifiler Row, sBit, len = [0, 0], 7 , 3
func (a *NgksiAndSpareHalfOctet) GetNasKeySetIdentifiler() (nasKeySetIdentifiler uint8) {
    return a.Octet & GetBitMask(7, 4)
}

// NgksiAndSpareHalfOctet 9.11.3.32 9.5
// NasKeySetIdentifiler Row, sBit, len = [0, 0], 7 , 3
func (a *NgksiAndSpareHalfOctet) SetNasKeySetIdentifiler(nasKeySetIdentifiler uint8) {
    a.Octet = (a.Octet & 143) + ((nasKeySetIdentifiler & 7) << 4)
}

// NgksiAndSpareHalfOctet 9.11.3.32 9.5
// SpareHalfOctet Row, sBit, len = [0, 0], 4 , 4
func (a *NgksiAndSpareHalfOctet) GetSpareHalfOctet() (spareHalfOctet uint8) {
    return a.Octet & GetBitMask(4, 0)
}

// NgksiAndSpareHalfOctet 9.11.3.32 9.5
// SpareHalfOctet Row, sBit, len = [0, 0], 4 , 4
func (a *NgksiAndSpareHalfOctet) SetSpareHalfOctet(spareHalfOctet uint8) {
    a.Octet = (a.Octet & 240) + (spareHalfOctet & 15)
}

