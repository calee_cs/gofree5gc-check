package nasType

// DeregistrationTypeAndSpareHalfOctet 9.11.3.20 9.5
// SwitchOff Row, sBit, len = [0, 0], 8 , 1
// ReRegistrationRequired Row, sBit, len = [0, 0], 7 , 1
// AccessType Row, sBit, len = [0, 0], 6 , 2
type DeregistrationTypeAndSpareHalfOctet struct {
	Octet uint8
}

func NewDeregistrationTypeAndSpareHalfOctet() (deregistrationTypeAndSpareHalfOctet *DeregistrationTypeAndSpareHalfOctet) {
    deregistrationTypeAndSpareHalfOctet = &DeregistrationTypeAndSpareHalfOctet{}
    return deregistrationTypeAndSpareHalfOctet
}

// DeregistrationTypeAndSpareHalfOctet 9.11.3.20 9.5
// SwitchOff Row, sBit, len = [0, 0], 8 , 1
func (a *DeregistrationTypeAndSpareHalfOctet) GetSwitchOff() (switchOff uint8) {
    return a.Octet & GetBitMask(8, 7)
}

// DeregistrationTypeAndSpareHalfOctet 9.11.3.20 9.5
// SwitchOff Row, sBit, len = [0, 0], 8 , 1
func (a *DeregistrationTypeAndSpareHalfOctet) SetSwitchOff(switchOff uint8) {
    a.Octet = (a.Octet & 127) + ((switchOff & 1) << 7)
}

// DeregistrationTypeAndSpareHalfOctet 9.11.3.20 9.5
// ReRegistrationRequired Row, sBit, len = [0, 0], 7 , 1
func (a *DeregistrationTypeAndSpareHalfOctet) GetReRegistrationRequired() (reRegistrationRequired uint8) {
    return a.Octet & GetBitMask(7, 6)
}

// DeregistrationTypeAndSpareHalfOctet 9.11.3.20 9.5
// ReRegistrationRequired Row, sBit, len = [0, 0], 7 , 1
func (a *DeregistrationTypeAndSpareHalfOctet) SetReRegistrationRequired(reRegistrationRequired uint8) {
    a.Octet = (a.Octet & 191) + ((reRegistrationRequired & 1) << 6)
}

// DeregistrationTypeAndSpareHalfOctet 9.11.3.20 9.5
// AccessType Row, sBit, len = [0, 0], 6 , 2
func (a *DeregistrationTypeAndSpareHalfOctet) GetAccessType() (accessType uint8) {
    return a.Octet & GetBitMask(6, 4)
}

// DeregistrationTypeAndSpareHalfOctet 9.11.3.20 9.5
// AccessType Row, sBit, len = [0, 0], 6 , 2
func (a *DeregistrationTypeAndSpareHalfOctet) SetAccessType(accessType uint8) {
    a.Octet = (a.Octet & 207) + ((accessType & 3) << 4)
}

