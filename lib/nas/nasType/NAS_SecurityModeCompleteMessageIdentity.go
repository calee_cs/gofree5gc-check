package nasType

// SecurityModeCompleteMessageIdentity 9.6
type SecurityModeCompleteMessageIdentity struct {
	Iei   uint8
	Octet uint8
}

func NewSecurityModeCompleteMessageIdentity(iei uint8) (securityModeCompleteMessageIdentity *SecurityModeCompleteMessageIdentity) {
    securityModeCompleteMessageIdentity = &SecurityModeCompleteMessageIdentity{}
    securityModeCompleteMessageIdentity.SetIei(iei)
    return securityModeCompleteMessageIdentity
}

// SecurityModeCompleteMessageIdentity 9.6
// Iei Row, sBit, len = [], 8, 8
func (a *SecurityModeCompleteMessageIdentity) GetIei() (iei uint8) {
    return a.Iei
}

// SecurityModeCompleteMessageIdentity 9.6
// Iei Row, sBit, len = [], 8, 8
func (a *SecurityModeCompleteMessageIdentity) SetIei(iei uint8) {
    a.Iei = iei
}

