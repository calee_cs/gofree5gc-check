package nasType

// IMEISV 9.11.3.4
// IdentityDigit1 Row, sBit, len = [0, 0], 8 , 4
// OddEvenIdic Row, sBit, len = [0, 0], 4 , 1
// TypeOfIdentity Row, sBit, len = [0, 0], 3 , 3
// IdentityDigitP_1 Row, sBit, len = [1, 1], 8 , 4
// IdentityDigitP Row, sBit, len = [1, 1], 4 , 4
type IMEISV struct {
	Iei   uint8
	Len   uint16
	Octet [2]uint8
}

func NewIMEISV(iei uint8) (iMEISV *IMEISV) {
    iMEISV = &IMEISV{}
    iMEISV.SetIei(iei)
    return iMEISV
}

// IMEISV 9.11.3.4
// Iei Row, sBit, len = [], 8, 8
func (a *IMEISV) GetIei() (iei uint8) {
    return a.Iei
}

// IMEISV 9.11.3.4
// Iei Row, sBit, len = [], 8, 8
func (a *IMEISV) SetIei(iei uint8) {
    a.Iei = iei
}

// IMEISV 9.11.3.4
// Len Row, sBit, len = [], 8, 16
func (a *IMEISV) GetLen() (len uint16) {
    return a.Len
}

// IMEISV 9.11.3.4
// Len Row, sBit, len = [], 8, 16
func (a *IMEISV) SetLen(len uint16) {
    a.Len = len
}

// IMEISV 9.11.3.4
// IdentityDigit1 Row, sBit, len = [0, 0], 8 , 4
func (a *IMEISV) GetIdentityDigit1() (identityDigit1 uint8) {
    return a.Octet[0] & GetBitMask(8, 4)
}

// IMEISV 9.11.3.4
// IdentityDigit1 Row, sBit, len = [0, 0], 8 , 4
func (a *IMEISV) SetIdentityDigit1(identityDigit1 uint8) {
    a.Octet[0] = (a.Octet[0] & 15) + ((identityDigit1 & 15) << 4)
}

// IMEISV 9.11.3.4
// OddEvenIdic Row, sBit, len = [0, 0], 4 , 1
func (a *IMEISV) GetOddEvenIdic() (oddEvenIdic uint8) {
    return a.Octet[0] & GetBitMask(4, 3)
}

// IMEISV 9.11.3.4
// OddEvenIdic Row, sBit, len = [0, 0], 4 , 1
func (a *IMEISV) SetOddEvenIdic(oddEvenIdic uint8) {
    a.Octet[0] = (a.Octet[0] & 247) + ((oddEvenIdic & 1) << 3)
}

// IMEISV 9.11.3.4
// TypeOfIdentity Row, sBit, len = [0, 0], 3 , 3
func (a *IMEISV) GetTypeOfIdentity() (typeOfIdentity uint8) {
    return a.Octet[0] & GetBitMask(3, 0)
}

// IMEISV 9.11.3.4
// TypeOfIdentity Row, sBit, len = [0, 0], 3 , 3
func (a *IMEISV) SetTypeOfIdentity(typeOfIdentity uint8) {
    a.Octet[0] = (a.Octet[0] & 248) + (typeOfIdentity & 7)
}

// IMEISV 9.11.3.4
// IdentityDigitP_1 Row, sBit, len = [1, 1], 8 , 4
func (a *IMEISV) GetIdentityDigitP_1() (identityDigitP_1 uint8) {
    return a.Octet[1] & GetBitMask(8, 4)
}

// IMEISV 9.11.3.4
// IdentityDigitP_1 Row, sBit, len = [1, 1], 8 , 4
func (a *IMEISV) SetIdentityDigitP_1(identityDigitP_1 uint8) {
    a.Octet[1] = (a.Octet[1] & 15) + ((identityDigitP_1 & 15) << 4)
}

// IMEISV 9.11.3.4
// IdentityDigitP Row, sBit, len = [1, 1], 4 , 4
func (a *IMEISV) GetIdentityDigitP() (identityDigitP uint8) {
    return a.Octet[1] & GetBitMask(4, 0)
}

// IMEISV 9.11.3.4
// IdentityDigitP Row, sBit, len = [1, 1], 4 , 4
func (a *IMEISV) SetIdentityDigitP(identityDigitP uint8) {
    a.Octet[1] = (a.Octet[1] & 240) + (identityDigitP & 15)
}

