package nasType

// IdentityTypeAndSpareHalfOctet 9.11.3.3 9.5
// TypeOfIdentity Row, sBit, len = [0, 0], 7 , 3
type IdentityTypeAndSpareHalfOctet struct {
	Octet uint8
}

func NewIdentityTypeAndSpareHalfOctet() (identityTypeAndSpareHalfOctet *IdentityTypeAndSpareHalfOctet) {
    identityTypeAndSpareHalfOctet = &IdentityTypeAndSpareHalfOctet{}
    return identityTypeAndSpareHalfOctet
}

// IdentityTypeAndSpareHalfOctet 9.11.3.3 9.5
// TypeOfIdentity Row, sBit, len = [0, 0], 7 , 3
func (a *IdentityTypeAndSpareHalfOctet) GetTypeOfIdentity() (typeOfIdentity uint8) {
    return a.Octet & GetBitMask(7, 4)
}

// IdentityTypeAndSpareHalfOctet 9.11.3.3 9.5
// TypeOfIdentity Row, sBit, len = [0, 0], 7 , 3
func (a *IdentityTypeAndSpareHalfOctet) SetTypeOfIdentity(typeOfIdentity uint8) {
    a.Octet = (a.Octet & 143) + ((typeOfIdentity & 7) << 4)
}

