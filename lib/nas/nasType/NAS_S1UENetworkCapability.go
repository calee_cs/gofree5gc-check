package nasType

// S1UENetworkCapability 9.11.3.48
// RES Row, sBit, len = [0, 15], 8 , 128
type S1UENetworkCapability struct {
	Iei   uint8
	Len   uint8
	Octet [16]uint8
}

func NewS1UENetworkCapability(iei uint8) (s1UENetworkCapability *S1UENetworkCapability) {
    s1UENetworkCapability = &S1UENetworkCapability{}
    s1UENetworkCapability.SetIei(iei)
    return s1UENetworkCapability
}

// S1UENetworkCapability 9.11.3.48
// Iei Row, sBit, len = [], 8, 8
func (a *S1UENetworkCapability) GetIei() (iei uint8) {
    return a.Iei
}

// S1UENetworkCapability 9.11.3.48
// Iei Row, sBit, len = [], 8, 8
func (a *S1UENetworkCapability) SetIei(iei uint8) {
    a.Iei = iei
}

// S1UENetworkCapability 9.11.3.48
// Len Row, sBit, len = [], 8, 8
func (a *S1UENetworkCapability) GetLen() (len uint8) {
    return a.Len
}

// S1UENetworkCapability 9.11.3.48
// Len Row, sBit, len = [], 8, 8
func (a *S1UENetworkCapability) SetLen(len uint8) {
    a.Len = len
}

// S1UENetworkCapability 9.11.3.48
// RES Row, sBit, len = [0, 15], 8 , 128
func (a *S1UENetworkCapability) GetRES() (rES [16]uint8) {
    copy(rES[:], a.Octet[0:16])
    return rES
}

// S1UENetworkCapability 9.11.3.48
// RES Row, sBit, len = [0, 15], 8 , 128
func (a *S1UENetworkCapability) SetRES(rES [16]uint8) {
    copy(a.Octet[0:16], rES[:])
}

