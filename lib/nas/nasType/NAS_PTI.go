package nasType

// PTI 9.6
// PDUSessionIdentity Row, sBit, len = [0, 0], 8 , 8
type PTI struct {
	Octet uint8
}

func NewPTI() (pTI *PTI) {
    pTI = &PTI{}
    return pTI
}

// PTI 9.6
// PDUSessionIdentity Row, sBit, len = [0, 0], 8 , 8
func (a *PTI) GetPDUSessionIdentity() (pDUSessionIdentity uint8) {
    return a.Octet
}

// PTI 9.6
// PDUSessionIdentity Row, sBit, len = [0, 0], 8 , 8
func (a *PTI) SetPDUSessionIdentity(pDUSessionIdentity uint8) {
    a.Octet = pDUSessionIdentity
}

