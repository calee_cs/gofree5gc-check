package nasType

// SelectedPDUSessionTypeAndSelectedSSCMode 9.11.4.11 9.11.4.16
// PDUSessionType  Row, sBit, len = [0, 0], 7 , 3
// SSCMode Row, sBit, len = [0, 0], 3 , 3
type SelectedPDUSessionTypeAndSelectedSSCMode struct {
	Octet uint8
}

func NewSelectedPDUSessionTypeAndSelectedSSCMode() (selectedPDUSessionTypeAndSelectedSSCMode *SelectedPDUSessionTypeAndSelectedSSCMode) {
    selectedPDUSessionTypeAndSelectedSSCMode = &SelectedPDUSessionTypeAndSelectedSSCMode{}
    return selectedPDUSessionTypeAndSelectedSSCMode
}

// SelectedPDUSessionTypeAndSelectedSSCMode 9.11.4.11 9.11.4.16
// PDUSessionType Row, sBit, len = [0, 0], 7 , 3
func (a *SelectedPDUSessionTypeAndSelectedSSCMode) GetPDUSessionType() (pDUSessionType uint8) {
    return a.Octet & GetBitMask(7, 4)
}

// SelectedPDUSessionTypeAndSelectedSSCMode 9.11.4.11 9.11.4.16
// PDUSessionType Row, sBit, len = [0, 0], 7 , 3
func (a *SelectedPDUSessionTypeAndSelectedSSCMode) SetPDUSessionType(pDUSessionType uint8) {
    a.Octet = (a.Octet & 143) + ((pDUSessionType & 7) << 4)
}

// SelectedPDUSessionTypeAndSelectedSSCMode 9.11.4.11 9.11.4.16
// SSCMode Row, sBit, len = [0, 0], 3 , 3
func (a *SelectedPDUSessionTypeAndSelectedSSCMode) GetSSCMode() (sSCMode uint8) {
    return a.Octet & GetBitMask(3, 0)
}

// SelectedPDUSessionTypeAndSelectedSSCMode 9.11.4.11 9.11.4.16
// SSCMode Row, sBit, len = [0, 0], 3 , 3
func (a *SelectedPDUSessionTypeAndSelectedSSCMode) SetSSCMode(sSCMode uint8) {
    a.Octet = (a.Octet & 248) + (sSCMode & 7)
}

