package nas

import (
	"bytes"
	"encoding/hex"
	"testing"
)

var hexString = "0741020bf600f110000201030003e605f07000001000050215d011d15200f11030395c0a003103e5e0349011035758a65d0100e0c1"

func TestNasGmmMessage(t *testing.T) {
	data, _ := hex.DecodeString(hexString)
	m := NewMessage()
	m.GmmMessageDecode(&data)

	buff := new(bytes.Buffer)
	m.GmmMessageEncode(buff)

}

func TestNasGsmMessage(t *testing.T) {
	data, _ := hex.DecodeString(hexString)
	m := NewMessage()
	m.GsmMessageDecode(&data)

	buff := new(bytes.Buffer)
	m.GsmMessageEncode(buff)
}
