package nasMessage

import (
        "bytes"
        "encoding/binary"
        "fmt"
        "gofree5gc/lib/nas/nasType"
)
type IdentityRequest struct {
    nasType.ExtendedProtocolDiscriminator
    nasType.SecurityHeaderTypeAndSpareHalfOctet
    nasType.IdentityRequestMessageIdentity
    nasType.IdentityTypeAndSpareHalfOctet
}

func NewIdentityRequest(iei uint8) (identityRequest *IdentityRequest) {
    identityRequest = &IdentityRequest{}
    return identityRequest
}

func (a *IdentityRequest) EncodeIdentityRequest(buffer *bytes.Buffer) {
    binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Write(buffer, binary.BigEndian, &a.SecurityHeaderTypeAndSpareHalfOctet)
    binary.Write(buffer, binary.BigEndian, &a.IdentityRequestMessageIdentity)
    binary.Write(buffer, binary.BigEndian, &a.IdentityTypeAndSpareHalfOctet)
}

func (a *IdentityRequest) DecodeIdentityRequest(byteArray *[]byte) {
    buffer := bytes.NewBuffer(*byteArray)
    binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Read(buffer, binary.BigEndian, &a.SecurityHeaderTypeAndSpareHalfOctet)
    binary.Read(buffer, binary.BigEndian, &a.IdentityRequestMessageIdentity)
    binary.Read(buffer, binary.BigEndian, &a.IdentityTypeAndSpareHalfOctet)
    for buffer.Len() > 0 {
        var ieiN uint8
        var tmpIeiN uint8
        binary.Read(buffer, binary.BigEndian, &ieiN)
        fmt.Println(ieiN)
        if ieiN >= 0x80 {
            tmpIeiN = ieiN & 0xf0
        } else {
            tmpIeiN = ieiN
        }
        fmt.Println("type", tmpIeiN)
        switch tmpIeiN  {
        default:
        }
    }
}

