package nasMessage

import (
        "bytes"
        "encoding/binary"
        "fmt"
        "gofree5gc/lib/nas/nasType"
)
type PDUSessionReleaseRequest struct {
    nasType.ExtendedProtocolDiscriminator
    nasType.PDUSessionID
    nasType.PTI
    nasType.PDUSESSIONRELEASEREQUESTMessageIdentity
    *nasType.Cause5GSM
    *nasType.ExtendedProtocolConfigurationOptions
}

func NewPDUSessionReleaseRequest(iei uint8) (pDUSessionReleaseRequest *PDUSessionReleaseRequest) {
    pDUSessionReleaseRequest = &PDUSessionReleaseRequest{}
    return pDUSessionReleaseRequest
}

const (
    PDUSessionReleaseRequestCause5GSMType uint8 = 0x59
    PDUSessionReleaseRequestExtendedProtocolConfigurationOptionsType uint8 = 0x7B
)

func (a *PDUSessionReleaseRequest) EncodePDUSessionReleaseRequest(buffer *bytes.Buffer) {
    binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Write(buffer, binary.BigEndian, &a.PDUSessionID)
    binary.Write(buffer, binary.BigEndian, &a.PTI)
    binary.Write(buffer, binary.BigEndian, &a.PDUSESSIONRELEASEREQUESTMessageIdentity)
    if a.Cause5GSM != nil {
        binary.Write(buffer, binary.BigEndian, a.Cause5GSM.GetIei())
        binary.Write(buffer, binary.BigEndian, &a.Cause5GSM.Octet)
    }
    if a.ExtendedProtocolConfigurationOptions != nil {
        binary.Write(buffer, binary.BigEndian, a.ExtendedProtocolConfigurationOptions.GetIei())
        binary.Write(buffer, binary.BigEndian, a.ExtendedProtocolConfigurationOptions.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Buffer)
    }
}

func (a *PDUSessionReleaseRequest) DecodePDUSessionReleaseRequest(byteArray *[]byte) {
    buffer := bytes.NewBuffer(*byteArray)
    binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Read(buffer, binary.BigEndian, &a.PDUSessionID)
    binary.Read(buffer, binary.BigEndian, &a.PTI)
    binary.Read(buffer, binary.BigEndian, &a.PDUSESSIONRELEASEREQUESTMessageIdentity)
    for buffer.Len() > 0 {
        var ieiN uint8
        var tmpIeiN uint8
        binary.Read(buffer, binary.BigEndian, &ieiN)
        fmt.Println(ieiN)
        if ieiN >= 0x80 {
            tmpIeiN = ieiN & 0xf0
        } else {
            tmpIeiN = ieiN
        }
        fmt.Println("type", tmpIeiN)
        switch tmpIeiN  {
        case PDUSessionReleaseRequestCause5GSMType:
            a.Cause5GSM = nasType.NewCause5GSM(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.Cause5GSM.Octet)
        case PDUSessionReleaseRequestExtendedProtocolConfigurationOptionsType:
            a.ExtendedProtocolConfigurationOptions = nasType.NewExtendedProtocolConfigurationOptions(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Len)
            a.ExtendedProtocolConfigurationOptions.SetLen(a.ExtendedProtocolConfigurationOptions.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Buffer)
        default:
        }
    }
}

