package nasMessage

import (
        "bytes"
        "encoding/binary"
        "fmt"
        "gofree5gc/lib/nas/nasType"
)
type SecurityModeComplete struct {
    nasType.ExtendedProtocolDiscriminator
    nasType.SecurityHeaderTypeAndSpareHalfOctet
    nasType.SecurityModeCompleteMessageIdentity
    *nasType.IMEISV
    *nasType.NASMessageContainer
}

func NewSecurityModeComplete(iei uint8) (securityModeComplete *SecurityModeComplete) {
    securityModeComplete = &SecurityModeComplete{}
    return securityModeComplete
}

const (
    SecurityModeCompleteIMEISVType uint8 = 0x77
    SecurityModeCompleteNASMessageContainerType uint8 = 0x71
)

func (a *SecurityModeComplete) EncodeSecurityModeComplete(buffer *bytes.Buffer) {
    binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Write(buffer, binary.BigEndian, &a.SecurityHeaderTypeAndSpareHalfOctet)
    binary.Write(buffer, binary.BigEndian, &a.SecurityModeCompleteMessageIdentity)
    if a.IMEISV != nil {
        binary.Write(buffer, binary.BigEndian, a.IMEISV.GetIei())
        binary.Write(buffer, binary.BigEndian, a.IMEISV.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.IMEISV.Octet)
    }
    if a.NASMessageContainer != nil {
        binary.Write(buffer, binary.BigEndian, a.NASMessageContainer.GetIei())
        binary.Write(buffer, binary.BigEndian, a.NASMessageContainer.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.NASMessageContainer.Buffer)
    }
}

func (a *SecurityModeComplete) DecodeSecurityModeComplete(byteArray *[]byte) {
    buffer := bytes.NewBuffer(*byteArray)
    binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Read(buffer, binary.BigEndian, &a.SecurityHeaderTypeAndSpareHalfOctet)
    binary.Read(buffer, binary.BigEndian, &a.SecurityModeCompleteMessageIdentity)
    for buffer.Len() > 0 {
        var ieiN uint8
        var tmpIeiN uint8
        binary.Read(buffer, binary.BigEndian, &ieiN)
        fmt.Println(ieiN)
        if ieiN >= 0x80 {
            tmpIeiN = ieiN & 0xf0
        } else {
            tmpIeiN = ieiN
        }
        fmt.Println("type", tmpIeiN)
        switch tmpIeiN  {
        case SecurityModeCompleteIMEISVType:
            a.IMEISV = nasType.NewIMEISV(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.IMEISV.Len)
            a.IMEISV.SetLen(a.IMEISV.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.IMEISV.Octet)
        case SecurityModeCompleteNASMessageContainerType:
            a.NASMessageContainer = nasType.NewNASMessageContainer(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.NASMessageContainer.Len)
            a.NASMessageContainer.SetLen(a.NASMessageContainer.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.NASMessageContainer.Buffer)
        default:
        }
    }
}

