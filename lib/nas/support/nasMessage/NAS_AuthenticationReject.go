package nasMessage

import (
        "bytes"
        "encoding/binary"
        "fmt"
        "gofree5gc/lib/nas/nasType"
)
type AuthenticationReject struct {
    nasType.ExtendedProtocolDiscriminator
    nasType.SecurityHeaderTypeAndSpareHalfOctet
    nasType.AuthenticationRejectMessageIdentity
    *nasType.EAPMessage
}

func NewAuthenticationReject(iei uint8) (authenticationReject *AuthenticationReject) {
    authenticationReject = &AuthenticationReject{}
    return authenticationReject
}

const (
    AuthenticationRejectEAPMessageType uint8 = 0x78
)

func (a *AuthenticationReject) EncodeAuthenticationReject(buffer *bytes.Buffer) {
    binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Write(buffer, binary.BigEndian, &a.SecurityHeaderTypeAndSpareHalfOctet)
    binary.Write(buffer, binary.BigEndian, &a.AuthenticationRejectMessageIdentity)
    if a.EAPMessage != nil {
        binary.Write(buffer, binary.BigEndian, a.EAPMessage.GetIei())
        binary.Write(buffer, binary.BigEndian, a.EAPMessage.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.EAPMessage.Buffer)
    }
}

func (a *AuthenticationReject) DecodeAuthenticationReject(byteArray *[]byte) {
    buffer := bytes.NewBuffer(*byteArray)
    binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Read(buffer, binary.BigEndian, &a.SecurityHeaderTypeAndSpareHalfOctet)
    binary.Read(buffer, binary.BigEndian, &a.AuthenticationRejectMessageIdentity)
    for buffer.Len() > 0 {
        var ieiN uint8
        var tmpIeiN uint8
        binary.Read(buffer, binary.BigEndian, &ieiN)
        fmt.Println(ieiN)
        if ieiN >= 0x80 {
            tmpIeiN = ieiN & 0xf0
        } else {
            tmpIeiN = ieiN
        }
        fmt.Println("type", tmpIeiN)
        switch tmpIeiN  {
        case AuthenticationRejectEAPMessageType:
            a.EAPMessage = nasType.NewEAPMessage(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.EAPMessage.Len)
            a.EAPMessage.SetLen(a.EAPMessage.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.EAPMessage.Buffer)
        default:
        }
    }
}

