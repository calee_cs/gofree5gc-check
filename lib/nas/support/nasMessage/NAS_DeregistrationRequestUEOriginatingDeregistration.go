package nasMessage

import (
        "bytes"
        "encoding/binary"
        "fmt"
        "gofree5gc/lib/nas/nasType"
)
type DeregistrationRequestUEOriginatingDeregistration struct {
    nasType.ExtendedProtocolDiscriminator
    nasType.SecurityHeaderTypeAndSpareHalfOctet
    nasType.DeregistrationRequestMessageIdentity
    nasType.DeregistrationTypeAndNgksi
    nasType.MobileIdentity5GS
}

func NewDeregistrationRequestUEOriginatingDeregistration(iei uint8) (deregistrationRequestUEOriginatingDeregistration *DeregistrationRequestUEOriginatingDeregistration) {
    deregistrationRequestUEOriginatingDeregistration = &DeregistrationRequestUEOriginatingDeregistration{}
    return deregistrationRequestUEOriginatingDeregistration
}

func (a *DeregistrationRequestUEOriginatingDeregistration) EncodeDeregistrationRequestUEOriginatingDeregistration(buffer *bytes.Buffer) {
    binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Write(buffer, binary.BigEndian, &a.SecurityHeaderTypeAndSpareHalfOctet)
    binary.Write(buffer, binary.BigEndian, &a.DeregistrationRequestMessageIdentity)
    binary.Write(buffer, binary.BigEndian, &a.DeregistrationTypeAndNgksi)
    binary.Write(buffer, binary.BigEndian, a.MobileIdentity5GS.GetLen())
    binary.Write(buffer, binary.BigEndian, &a.MobileIdentity5GS.Buffer)
}

func (a *DeregistrationRequestUEOriginatingDeregistration) DecodeDeregistrationRequestUEOriginatingDeregistration(byteArray *[]byte) {
    buffer := bytes.NewBuffer(*byteArray)
    binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Read(buffer, binary.BigEndian, &a.SecurityHeaderTypeAndSpareHalfOctet)
    binary.Read(buffer, binary.BigEndian, &a.DeregistrationRequestMessageIdentity)
    binary.Read(buffer, binary.BigEndian, &a.DeregistrationTypeAndNgksi)
    binary.Read(buffer, binary.BigEndian, &a.MobileIdentity5GS.Len)
    a.MobileIdentity5GS.SetLen(a.MobileIdentity5GS.GetLen())
    binary.Write(buffer, binary.BigEndian, &a.MobileIdentity5GS.Buffer)
    for buffer.Len() > 0 {
        var ieiN uint8
        var tmpIeiN uint8
        binary.Read(buffer, binary.BigEndian, &ieiN)
        fmt.Println(ieiN)
        if ieiN >= 0x80 {
            tmpIeiN = ieiN & 0xf0
        } else {
            tmpIeiN = ieiN
        }
        fmt.Println("type", tmpIeiN)
        switch tmpIeiN  {
        default:
        }
    }
}

