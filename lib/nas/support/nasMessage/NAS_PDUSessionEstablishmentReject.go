package nasMessage

import (
        "bytes"
        "encoding/binary"
        "fmt"
        "gofree5gc/lib/nas/nasType"
)
type PDUSessionEstablishmentReject struct {
    nasType.ExtendedProtocolDiscriminator
    nasType.PDUSessionID
    nasType.PTI
    nasType.PDUSESSIONESTABLISHMENTREJECTMessageIdentity
    nasType.Cause5GSM
    *nasType.BackoffTimerValue
    *nasType.AllowedSSCMode
    *nasType.EAPMessage
    *nasType.ExtendedProtocolConfigurationOptions
}

func NewPDUSessionEstablishmentReject(iei uint8) (pDUSessionEstablishmentReject *PDUSessionEstablishmentReject) {
    pDUSessionEstablishmentReject = &PDUSessionEstablishmentReject{}
    return pDUSessionEstablishmentReject
}

const (
    PDUSessionEstablishmentRejectBackoffTimerValueType uint8 = 0x37
    PDUSessionEstablishmentRejectAllowedSSCModeType uint8 = 0xF0
    PDUSessionEstablishmentRejectEAPMessageType uint8 = 0x78
    PDUSessionEstablishmentRejectExtendedProtocolConfigurationOptionsType uint8 = 0x7B
)

func (a *PDUSessionEstablishmentReject) EncodePDUSessionEstablishmentReject(buffer *bytes.Buffer) {
    binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Write(buffer, binary.BigEndian, &a.PDUSessionID)
    binary.Write(buffer, binary.BigEndian, &a.PTI)
    binary.Write(buffer, binary.BigEndian, &a.PDUSESSIONESTABLISHMENTREJECTMessageIdentity)
    binary.Write(buffer, binary.BigEndian, &a.Cause5GSM)
    if a.BackoffTimerValue != nil {
        binary.Write(buffer, binary.BigEndian, a.BackoffTimerValue.GetIei())
        binary.Write(buffer, binary.BigEndian, a.BackoffTimerValue.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.BackoffTimerValue.Octet)
    }
    if a.AllowedSSCMode != nil {
        binary.Write(buffer, binary.BigEndian, a.AllowedSSCMode.GetIei())
        binary.Write(buffer, binary.BigEndian, &a.AllowedSSCMode.Octet)
    }
    if a.EAPMessage != nil {
        binary.Write(buffer, binary.BigEndian, a.EAPMessage.GetIei())
        binary.Write(buffer, binary.BigEndian, a.EAPMessage.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.EAPMessage.Buffer)
    }
    if a.ExtendedProtocolConfigurationOptions != nil {
        binary.Write(buffer, binary.BigEndian, a.ExtendedProtocolConfigurationOptions.GetIei())
        binary.Write(buffer, binary.BigEndian, a.ExtendedProtocolConfigurationOptions.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Buffer)
    }
}

func (a *PDUSessionEstablishmentReject) DecodePDUSessionEstablishmentReject(byteArray *[]byte) {
    buffer := bytes.NewBuffer(*byteArray)
    binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Read(buffer, binary.BigEndian, &a.PDUSessionID)
    binary.Read(buffer, binary.BigEndian, &a.PTI)
    binary.Read(buffer, binary.BigEndian, &a.PDUSESSIONESTABLISHMENTREJECTMessageIdentity)
    binary.Read(buffer, binary.BigEndian, &a.Cause5GSM)
    for buffer.Len() > 0 {
        var ieiN uint8
        var tmpIeiN uint8
        binary.Read(buffer, binary.BigEndian, &ieiN)
        fmt.Println(ieiN)
        if ieiN >= 0x80 {
            tmpIeiN = ieiN & 0xf0
        } else {
            tmpIeiN = ieiN
        }
        fmt.Println("type", tmpIeiN)
        switch tmpIeiN  {
        case PDUSessionEstablishmentRejectBackoffTimerValueType:
            a.BackoffTimerValue = nasType.NewBackoffTimerValue(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.BackoffTimerValue.Len)
            a.BackoffTimerValue.SetLen(a.BackoffTimerValue.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.BackoffTimerValue.Octet)
        case PDUSessionEstablishmentRejectAllowedSSCModeType:
            a.AllowedSSCMode = nasType.NewAllowedSSCMode(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.AllowedSSCMode.Octet)
        case PDUSessionEstablishmentRejectEAPMessageType:
            a.EAPMessage = nasType.NewEAPMessage(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.EAPMessage.Len)
            a.EAPMessage.SetLen(a.EAPMessage.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.EAPMessage.Buffer)
        case PDUSessionEstablishmentRejectExtendedProtocolConfigurationOptionsType:
            a.ExtendedProtocolConfigurationOptions = nasType.NewExtendedProtocolConfigurationOptions(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Len)
            a.ExtendedProtocolConfigurationOptions.SetLen(a.ExtendedProtocolConfigurationOptions.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolConfigurationOptions.Buffer)
        default:
        }
    }
}

