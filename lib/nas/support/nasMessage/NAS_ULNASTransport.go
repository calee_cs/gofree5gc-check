package nasMessage

import (
        "bytes"
        "encoding/binary"
        "fmt"
        "gofree5gc/lib/nas/nasType"
)
type ULNASTransport struct {
    nasType.ExtendedProtocolDiscriminator
    nasType.SecurityHeaderTypeAndSpareHalfOctet
    nasType.ULNASTRANSPORTMessageIdentity
    nasType.PayloadContainerTypeAndSpareHalfOctet
    nasType.PayloadContainer
    *nasType.PDUSessionID
    *nasType.OldPDUSessionID
    *nasType.RequestType
    *nasType.SNSSAI
    *nasType.AdditionalInformation
}

func NewULNASTransport(iei uint8) (uLNASTransport *ULNASTransport) {
    uLNASTransport = &ULNASTransport{}
    return uLNASTransport
}

const (
    ULNASTransportPDUSessionIDType uint8 = 0x12
    ULNASTransportOldPDUSessionIDType uint8 = 0x59
    ULNASTransportRequestTypeType uint8 = 0x80
    ULNASTransportSNSSAIType uint8 = 0x22
    ULNASTransportAdditionalInformationType uint8 = 0x24
)

func (a *ULNASTransport) EncodeULNASTransport(buffer *bytes.Buffer) {
    binary.Write(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Write(buffer, binary.BigEndian, &a.SecurityHeaderTypeAndSpareHalfOctet)
    binary.Write(buffer, binary.BigEndian, &a.ULNASTRANSPORTMessageIdentity)
    binary.Write(buffer, binary.BigEndian, &a.PayloadContainerTypeAndSpareHalfOctet)
    binary.Write(buffer, binary.BigEndian, a.PayloadContainer.GetLen())
    binary.Write(buffer, binary.BigEndian, &a.PayloadContainer.Buffer)
    if a.PDUSessionID != nil {
        binary.Write(buffer, binary.BigEndian, a.PDUSessionID.GetIei())
        binary.Write(buffer, binary.BigEndian, &a.PDUSessionID.Octet)
    }
    if a.OldPDUSessionID != nil {
        binary.Write(buffer, binary.BigEndian, a.OldPDUSessionID.GetIei())
        binary.Write(buffer, binary.BigEndian, &a.OldPDUSessionID.Octet)
    }
    if a.RequestType != nil {
        binary.Write(buffer, binary.BigEndian, a.RequestType.GetIei())
        binary.Write(buffer, binary.BigEndian, &a.RequestType.Octet)
    }
    if a.SNSSAI != nil {
        binary.Write(buffer, binary.BigEndian, a.SNSSAI.GetIei())
        binary.Write(buffer, binary.BigEndian, a.SNSSAI.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.SNSSAI.Octet)
    }
    if a.AdditionalInformation != nil {
        binary.Write(buffer, binary.BigEndian, a.AdditionalInformation.GetIei())
        binary.Write(buffer, binary.BigEndian, a.AdditionalInformation.GetLen())
        binary.Write(buffer, binary.BigEndian, &a.AdditionalInformation.Buffer)
    }
}

func (a *ULNASTransport) DecodeULNASTransport(byteArray *[]byte) {
    buffer := bytes.NewBuffer(*byteArray)
    binary.Read(buffer, binary.BigEndian, &a.ExtendedProtocolDiscriminator)
    binary.Read(buffer, binary.BigEndian, &a.SecurityHeaderTypeAndSpareHalfOctet)
    binary.Read(buffer, binary.BigEndian, &a.ULNASTRANSPORTMessageIdentity)
    binary.Read(buffer, binary.BigEndian, &a.PayloadContainerTypeAndSpareHalfOctet)
    binary.Read(buffer, binary.BigEndian, &a.PayloadContainer.Len)
    a.PayloadContainer.SetLen(a.PayloadContainer.GetLen())
    binary.Write(buffer, binary.BigEndian, &a.PayloadContainer.Buffer)
    for buffer.Len() > 0 {
        var ieiN uint8
        var tmpIeiN uint8
        binary.Read(buffer, binary.BigEndian, &ieiN)
        fmt.Println(ieiN)
        if ieiN >= 0x80 {
            tmpIeiN = ieiN & 0xf0
        } else {
            tmpIeiN = ieiN
        }
        fmt.Println("type", tmpIeiN)
        switch tmpIeiN  {
        case ULNASTransportOldPDUSessionIDType:
            a.OldPDUSessionID = nasType.NewOldPDUSessionID(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.OldPDUSessionID.Octet)
        case ULNASTransportRequestTypeType:
            a.RequestType = nasType.NewRequestType(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.RequestType.Octet)
        case ULNASTransportSNSSAIType:
            a.SNSSAI = nasType.NewSNSSAI(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.SNSSAI.Len)
            a.SNSSAI.SetLen(a.SNSSAI.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.SNSSAI.Octet)
        case ULNASTransportAdditionalInformationType:
            a.AdditionalInformation = nasType.NewAdditionalInformation(ieiN)
            binary.Read(buffer, binary.BigEndian, &a.AdditionalInformation.Len)
            a.AdditionalInformation.SetLen(a.AdditionalInformation.GetLen())
            binary.Read(buffer, binary.BigEndian, &a.AdditionalInformation.Buffer)
        default:
        }
    }
}

