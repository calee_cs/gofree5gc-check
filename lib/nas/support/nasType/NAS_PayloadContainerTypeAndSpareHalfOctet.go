package nasType

// PayloadContainerTypeAndSpareHalfOctet 9.11.3.40 9.5
// PayloadContainerType Row, sBit, len = [0, 0], 8 , 4
type PayloadContainerTypeAndSpareHalfOctet struct {
	Octet uint8
}

func NewPayloadContainerTypeAndSpareHalfOctet() (payloadContainerTypeAndSpareHalfOctet *PayloadContainerTypeAndSpareHalfOctet) {
    payloadContainerTypeAndSpareHalfOctet = &PayloadContainerTypeAndSpareHalfOctet{}
    return payloadContainerTypeAndSpareHalfOctet
}

// PayloadContainerTypeAndSpareHalfOctet 9.11.3.40 9.5
// PayloadContainerType Row, sBit, len = [0, 0], 8 , 4
func (a *PayloadContainerTypeAndSpareHalfOctet) GetPayloadContainerType() (payloadContainerType uint8) {
    return a.Octet & GetBitMask(8, 4)
}

// PayloadContainerTypeAndSpareHalfOctet 9.11.3.40 9.5
// PayloadContainerType Row, sBit, len = [0, 0], 8 , 4
func (a *PayloadContainerTypeAndSpareHalfOctet) SetPayloadContainerType(payloadContainerType uint8) {
    a.Octet = (a.Octet & 15) + ((payloadContainerType & 15) << 4)
}

