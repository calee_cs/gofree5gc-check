package nasType

// NgksiAndServiceType 9.11.3.32 9.11.3.50
// TSC Row, sBit, len = [0, 0], 8 , 1
// NasKeySetIdentifiler Row, sBit, len = [0, 0], 7 , 3
// ServiceTypeValue Row, sBit, len = [0, 0], 4 , 4
type NgksiAndServiceType struct {
	Octet uint8
}

func NewNgksiAndServiceType() (ngksiAndServiceType *NgksiAndServiceType) {
    ngksiAndServiceType = &NgksiAndServiceType{}
    return ngksiAndServiceType
}

// NgksiAndServiceType 9.11.3.32 9.11.3.50
// TSC Row, sBit, len = [0, 0], 8 , 1
func (a *NgksiAndServiceType) GetTSC() (tSC uint8) {
    return a.Octet & GetBitMask(8, 7)
}

// NgksiAndServiceType 9.11.3.32 9.11.3.50
// TSC Row, sBit, len = [0, 0], 8 , 1
func (a *NgksiAndServiceType) SetTSC(tSC uint8) {
    a.Octet = (a.Octet & 127) + ((tSC & 1) << 7)
}

// NgksiAndServiceType 9.11.3.32 9.11.3.50
// NasKeySetIdentifiler Row, sBit, len = [0, 0], 7 , 3
func (a *NgksiAndServiceType) GetNasKeySetIdentifiler() (nasKeySetIdentifiler uint8) {
    return a.Octet & GetBitMask(7, 4)
}

// NgksiAndServiceType 9.11.3.32 9.11.3.50
// NasKeySetIdentifiler Row, sBit, len = [0, 0], 7 , 3
func (a *NgksiAndServiceType) SetNasKeySetIdentifiler(nasKeySetIdentifiler uint8) {
    a.Octet = (a.Octet & 143) + ((nasKeySetIdentifiler & 7) << 4)
}

// NgksiAndServiceType 9.11.3.32 9.11.3.50
// ServiceTypeValue Row, sBit, len = [0, 0], 4 , 4
func (a *NgksiAndServiceType) GetServiceTypeValue() (serviceTypeValue uint8) {
    return a.Octet & GetBitMask(4, 0)
}

// NgksiAndServiceType 9.11.3.32 9.11.3.50
// ServiceTypeValue Row, sBit, len = [0, 0], 4 , 4
func (a *NgksiAndServiceType) SetServiceTypeValue(serviceTypeValue uint8) {
    a.Octet = (a.Octet & 240) + (serviceTypeValue & 15)
}

