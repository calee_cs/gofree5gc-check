package nasType

// AuthorizedQosFlowDescriptions 9.11.4.12
// QoSFlowDescription Row, sBit, len = [0, 0], 8 , INF
type AuthorizedQosFlowDescriptions struct {
	Iei    uint8
	Len    uint8
	Buffer []uint8
}

func NewAuthorizedQosFlowDescriptions(iei uint8) (authorizedQosFlowDescriptions *AuthorizedQosFlowDescriptions) {
    authorizedQosFlowDescriptions = &AuthorizedQosFlowDescriptions{}
    authorizedQosFlowDescriptions.SetIei(iei)
    return authorizedQosFlowDescriptions
}

// AuthorizedQosFlowDescriptions 9.11.4.12
// Iei Row, sBit, len = [], 8, 8
func (a *AuthorizedQosFlowDescriptions) GetIei() (iei uint8) {
    return a.Iei
}

// AuthorizedQosFlowDescriptions 9.11.4.12
// Iei Row, sBit, len = [], 8, 8
func (a *AuthorizedQosFlowDescriptions) SetIei(iei uint8) {
    a.Iei = iei
}

// AuthorizedQosFlowDescriptions 9.11.4.12
// Len Row, sBit, len = [], 8, 8
func (a *AuthorizedQosFlowDescriptions) GetLen() (len uint8) {
    return a.Len
}

// AuthorizedQosFlowDescriptions 9.11.4.12
// Len Row, sBit, len = [], 8, 8
func (a *AuthorizedQosFlowDescriptions) SetLen(len uint8) {
    a.Len = len
    a.Buffer = make([]uint8, a.Len)
}

// AuthorizedQosFlowDescriptions 9.11.4.12
// QoSFlowDescription Row, sBit, len = [0, 0], 8 , INF
func (a *AuthorizedQosFlowDescriptions) GetQoSFlowDescription() (qoSFlowDescription []uint8) {
    qoSFlowDescription = make([]uint8, len(a.Buffer))
    copy(qoSFlowDescription, a.Buffer)
    return qoSFlowDescription
}

// AuthorizedQosFlowDescriptions 9.11.4.12
// QoSFlowDescription Row, sBit, len = [0, 0], 8 , INF
func (a *AuthorizedQosFlowDescriptions) SetQoSFlowDescription(qoSFlowDescription []uint8) {
    copy(a.Buffer, qoSFlowDescription)
}

