package nasType

// SecurityHeaderTypeAndSpareHalfOctet 9.3 9.5
// SecurityHeaderType Row, sBit, len = [0, 0], 8 , 4
// SpareHalfOctet Row, sBit, len = [0, 0], 4 , 4
type SecurityHeaderTypeAndSpareHalfOctet struct {
	Octet uint8
}

func NewSecurityHeaderTypeAndSpareHalfOctet() (securityHeaderTypeAndSpareHalfOctet *SecurityHeaderTypeAndSpareHalfOctet) {
    securityHeaderTypeAndSpareHalfOctet = &SecurityHeaderTypeAndSpareHalfOctet{}
    return securityHeaderTypeAndSpareHalfOctet
}

// SecurityHeaderTypeAndSpareHalfOctet 9.3 9.5
// SecurityHeaderType Row, sBit, len = [0, 0], 8 , 4
func (a *SecurityHeaderTypeAndSpareHalfOctet) GetSecurityHeaderType() (securityHeaderType uint8) {
    return a.Octet & GetBitMask(8, 4)
}

// SecurityHeaderTypeAndSpareHalfOctet 9.3 9.5
// SecurityHeaderType Row, sBit, len = [0, 0], 8 , 4
func (a *SecurityHeaderTypeAndSpareHalfOctet) SetSecurityHeaderType(securityHeaderType uint8) {
    a.Octet = (a.Octet & 15) + ((securityHeaderType & 15) << 4)
}

// SecurityHeaderTypeAndSpareHalfOctet 9.3 9.5
// SpareHalfOctet Row, sBit, len = [0, 0], 4 , 4
func (a *SecurityHeaderTypeAndSpareHalfOctet) GetSpareHalfOctet() (spareHalfOctet uint8) {
    return a.Octet & GetBitMask(4, 0)
}

// SecurityHeaderTypeAndSpareHalfOctet 9.3 9.5
// SpareHalfOctet Row, sBit, len = [0, 0], 4 , 4
func (a *SecurityHeaderTypeAndSpareHalfOctet) SetSpareHalfOctet(spareHalfOctet uint8) {
    a.Octet = (a.Octet & 240) + (spareHalfOctet & 15)
}

